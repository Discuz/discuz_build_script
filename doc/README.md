# DiscuzQ 发版脚本使用说明

## 一. 依赖

### 1.1. 脚本依赖

- zip : 压缩build之后的dzq代码
- coscmd 工具 : 可以用来上传文件到cos [https://cloud.tencent.com/document/product/436/10976](https://cloud.tencent.com/document/product/436/10976)
- docker ：用于打包docker镜像

debian/ubuntu安装命令如下:

```shell
sudo apt-get install zip python pip jq docker-ce
pip install coscmd
```

### 1.2. 配置

生成coscmd配置文件

```shell
coscmd config \
# 腾讯云对象存储APPKEY
-a AKID******************************** \
# 腾讯云对象存储SECRET
-s jL************************* \
# 腾讯云对象存储BUCKET
-b dzq-125******* \
# 腾讯云对象存储区域
-r ap-guangzhou
```

登录docker官方仓库，或者三方仓库，用于上传打包生成的docker镜像

```shell
# $REGISTRY 代表仓库地址，可选，不指定则登录docker官方仓库
docker login --username=<用户名> --password=<密码> $REGISTRY
```

## 二. 版本发布

### 2.1. 概述

发版脚本中构建任务可以分步执行，也可以一次性执行，脚本包含两个关键的输入参数：

- `$TAG` : 第一个参数，发布的版本，需要在代码仓库上创建一个分支，并且把tag设置为该版本，如 `v3.0.220521`
- `$DRY_RUN` : 第二个参数，指定值为`-n`，该参数存在则在执行build相关任务之后。不会将build的源文件提交到git仓库、不会上传zip到cos、不会上传docker镜像到镜像仓库

### 2.2. 分步构建

- **合并多端代码（build_discuz）**

修改 `build_discuz` 文件，找到如下环境变量，设置为自己的git仓库地址

```shell
# DiscuzQ后端代码 仓库
DISCUZ_REPO="git@gitee.com:kotlindev/Discuz-Q.git"
# DiscuzQ前台代码 编译后的静态资源仓库
DISCUZ_WEB="git@gitee.com:kotlindev/dzq-default-web-static.git"
# DiscuzQ后台代码 编译后的静态资源仓库
DISCUZ_ADMIN="git@gitee.com:kotlindev/dzq-admin-static.git"
# 构建的之后的仓库
DISCUZ_BUILDED="git@gitee.com:kotlindev/dzq_builded.git"
```

执行脚本

```shell
chmod +x build_discuz
bash build_discuz $TAG
```

- **构建dzq框架代码（build_framework）**

修改 `build_framework` 文件，找到如下环境变量，设置为自己的git仓库地址

```shell
# framework 框架仓库
FRAMEWORK_REPO="git@gitee.com:kotlindev/Discuz-Q-Framework.git"
```

执行脚本

```shell
chmod +x build_framework
bash build_framework $TAG
```

- **构建zip安装包（build_vendor）**

修改 `build_vendor` 文件，找到如下环境变量，设置为自己的git仓库地址

```shell
# vendor 代码仓库
DZQ_VENDOR_REPO="git@gitee.com:kotlindev/dzq_vendor.git"
```

执行以下脚本进行构建

```shell
chmod +x build_vendor
bash build_vendor $TAG
```

- **构建docker镜像（build_docker）**

修改 `build_docker` 文件，找到如下环境变量，设置为自己的git仓库地址

```shell
# docker 镜像远端仓库
DOCKER_REPO="kotlindev/dzq"
# docker 无数据库镜像远端仓库
DOCKER_NO_DB_REPO="kotlindev/dzq-no-db"
```

合并框架代码

```shell
chmod +x build_docker
bash build_docker $TAG
```

### 2.3. 一次性构建

需修改 2.2 说明的相关参数后，执行一次性构建脚本即可

```shell
chmod +x build_all
bash build_all $TAG
```
