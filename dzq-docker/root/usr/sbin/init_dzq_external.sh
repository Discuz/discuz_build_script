#!/bin/sh


# 如果没有外部目录映射进来，什么都不做，退出 
[ ! -d "/var/lib/discuz" ] && exit 0

# 如果没有单独映射config目录，创建config目录
[ ! -d "/var/lib/discuz/config" ] && mkdir /var/lib/discuz/config
[ ! -d "/var/lib/discuz/storage" ] && mkdir /var/lib/discuz/storage

# 如果config还没有创建软链接
if [ ! -L "/var/www/discuz/config" ]; then
    # 还没有config_default.php，说明需要初始化
    if [ ! -f "/var/lib/discuz/config/config_default.php" ]; then 
        cp /var/www/discuz/config/* /var/lib/discuz/config/
    fi
    mv /var/www/discuz/config /var/www/discuz/config.bak
    ln -s /var/lib/discuz/config /var/www/discuz/config
    chown -R www-data:www-data /var/www/discuz/config /var/lib/discuz/config
fi

# 如果storage还没创建软链接 
if [ ! -L "/var/www/discuz/storage" ]; then
    # 还没有app目录，说明需要初始化
    if [ ! -d "/var/lib/discuz/storage/app" ]; then 
        cp -r /var/www/discuz/storage/* /var/lib/discuz/storage/
    fi
    mv /var/www/discuz/storage /var/www/discuz/storage.bak
    ln -s /var/lib/discuz/storage /var/www/discuz/storage
    rm -f /var/www/discuz/public/storage
    ln -s /var/www/discuz/storage/app/public /var/www/discuz/public/storage
    chown -R www-data:www-data /var/www/discuz/storage /var/lib/discuz/storage /var/www/discuz/public/storage
fi
